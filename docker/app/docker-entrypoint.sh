#!/bin/bash

set -e
BUNDLE_IGNORE_CONFIG=1 bundle install

# Wait for MySQL
until mysql -h $DB_HOST -P $DB_PORT -p$DB_PASSWORD --execute "SHOW DATABASES;"; do 
 sleep 1
done

bundle exec rake db:migrate 2>/dev/null || bundle exec rake db:setup

exec "/bin/bash"
