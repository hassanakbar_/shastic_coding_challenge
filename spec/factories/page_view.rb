# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :page_view do
    url { Faker::Internet.url }
    title { Faker::Lorem.sentence(word_count: 3) }
    time_spent { Faker::Number.non_zero_digit }
    timestamp { Faker::Number.between(from: 1, to: 100_000) }

    association :visit, factory: :visit
  end
end
