# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :visit do
    evid { 'evid_ab7cde02-9523-109d-bbb6-c7e845a5b433' }
    vendor_site_id { Faker::Number.non_zero_digit }
    vendor_visit_id { Faker::Number.between(from: 1, to: 100_000) }
    visit_ip { Faker::Internet.ip_v4_address }
    vendor_visitor_id { Faker::Alphanumeric.alpha(number: 15) }

    trait :incorret_evid do
      evid { 'Incorrect_evid' }
    end
  end
end
