# frozen_string_literal: true

require 'spec_helper'
require 'json'

RSpec.describe Visits::CreateService do
  describe '#call' do
    context 'params are valid' do
      let(:visit_params) { JSON.parse(File.read('samples/api_response.json'))[0] }
      let(:create_service) { described_class.new(visit_params) }

      it 'creates visit record' do
        expect { create_service.call }.to change(Visit, :count).by(1)
      end

      it 'removes duplicate pages' do
        total_page_views_count = visit_params['actionDetails'].count
        uniq_page_count = visit_params['actionDetails'].uniq { |p| p['pageId'] }.count
        create_service.call

        expect(PageView.count).to eq(uniq_page_count)
        expect(PageView.count).to be < total_page_views_count
      end
    end

    context 'evid is invalid' do
      let(:visit_params) { JSON.parse(File.read('samples/api_response.json'))[1] }
      let(:create_service) { described_class.new(visit_params) }

      it 'returns error in result' do
        result = create_service.call
        expect(result[:success]).to be_falsey
        expect(result[:message]).to include('Evid is invalid')
      end
    end
  end
end
