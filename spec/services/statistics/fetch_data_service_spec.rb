# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Statistics::FetchDataService, type: :request do
  describe '#call' do
    subject(:data_service) { described_class.new }

    context 'api resturns request timeout' do
      it 'returns error in result' do
        stub_request(:get, URI(ENV['SHASTIC_API_SERVER_URL']))
          .to_timeout
        result = data_service.call
        expect(result[:success]).to be_falsey
        expect(result[:message]).to eq 'Request Timeout!'
      end
    end

    context 'api resturns invalid json' do
      it 'returns error in result' do
        stub_request(:get, URI(ENV['SHASTIC_API_SERVER_URL']))
          .to_return status: 200, body: 'abc'
        result = data_service.call
        expect(result[:success]).to be_falsey
        expect(result[:message]).to eq 'Invalid Response!'
      end
    end

    context 'api resturns valid response' do
      it 'returns data in result' do
        stub_request(:get, URI(ENV['SHASTIC_API_SERVER_URL']))
          .to_return status: 200, body: File.read('samples/api_response.json')
        result = data_service.call
        expect(result[:success]).to be_truthy
        expect(result[:data]).to be_kind_of Array
      end
    end
  end
end
