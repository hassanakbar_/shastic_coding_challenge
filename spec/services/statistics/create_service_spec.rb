# frozen_string_literal: true

require 'spec_helper'
require 'json'

RSpec.describe Statistics::CreateService do
  describe '#call' do
    context 'api statistics are passed' do
      let(:api_response) { JSON.parse(File.read('samples/api_response.json')) }
      let(:create_service) { described_class.new(api_response) }

      it 'saves valid records' do
        expect { create_service.call }.to change(Visit, :count).by(2)
      end
    end
  end
end
