# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Visit, type: :model do
  describe 'validations' do
    it { is_expected.to allow_value('evid_966634dc-0bf6-1ff7-f4b6-08000cs95c67').for(:evid) }

    context 'with invalid evid' do
      let(:visit) { build(:visit, :incorret_evid) }

      it { expect(visit).to be_invalid }

      it 'has error on evid' do
        visit.save
        expect(visit.errors.to_h).to eq(evid: 'is invalid')
      end
    end
  end

  describe 'associations' do
    it { is_expected.to have_many(:page_views) }
  end
end
