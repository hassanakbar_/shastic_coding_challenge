# frozen_string_literal: true

require 'spec_helper'

RSpec.describe PageView, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:visit) }
  end

  describe 'sorting order' do
    let!(:page_view_1) { create(:page_view, timestamp: 1_600_956_270) }
    let!(:page_view_2) { create(:page_view, timestamp: 1_600_956_257) }
    let!(:page_view_3) { create(:page_view, timestamp: 1_600_956_259) }

    it 'sorted by timestamp in ascending order' do
      page_views = described_class.all.to_a
      expected_order = [page_view_2, page_view_3, page_view_1]
      expect(page_views).to eq(expected_order)
    end
  end
end
