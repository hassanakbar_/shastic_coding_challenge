# frozen_string_literal: true

require 'active_record'
require 'erubis'

db_configuration = YAML.load(Erubis::Eruby.new(IO.read('db/config.yml')).result)

ActiveRecord::Base.establish_connection(
  db_configuration[ENV['RACK_ENV'] || 'development']
)

Dir['/app/app/**/*.rb'].sort.each { |file| require file }
