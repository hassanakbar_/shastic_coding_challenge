# frozen_string_literal: true

source 'https://rubygems.org'

# Database manager
gem 'activerecord'
gem 'mysql2', '= 0.5.3'
gem 'standalone_migrations'

# make-like build utility for Ruby
gem 'rake'

# For parsing ruby code in yml
gem 'erubis'

group :development, :test do
  # Installed for both envs in order to run seed
  gem 'factory_bot', '~> 5.2.0'
  gem 'faker', '~> 2.13.0'

  gem 'pry'
  gem 'rspec', '~> 3.5'
end

group :development do
  gem 'rubocop', require: false
  gem 'rubocop-performance', '~> 1.6.1', require: false
  gem 'rubocop-rspec', require: false
end

group :test do
  gem 'database_cleaner-active_record'
  gem 'shoulda-matchers', '~> 4.0'
  gem 'simplecov', require: false
  gem 'webmock'
end
