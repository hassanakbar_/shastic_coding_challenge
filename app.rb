# frozen_string_literal: true

require_relative 'config/environment'

# Driver class
class Driver
  def self.call
    puts "#{'-' * 10} Fetching data from API Server #{'-' * 10}"
    api_result = ::Statistics::FetchDataService.new.call
    if api_result[:success]
      puts "#{'-' * 10} Creating statistics #{'-' * 10}"
      result = ::Statistics::CreateService.new(api_result[:data]).call
      puts "COMPLETED! #{result[:data][:success_count]} out of #{result[:data][:total]} visits saved!"
      puts "FAILURE REASONS: #{result[:data][:failure_reasons].join(', ')}" if result[:data][:failure_reasons].any?
    else
      puts "Data fetching failed #{'-' * 10}"
      puts "REASON: #{api_result[:message]}"
    end
  end
end

Driver.call
