# frozen_string_literal: true

require 'net/http'
require 'json'

module Statistics
  class FetchDataService
    def call
      response = Net::HTTP.get URI(ENV['SHASTIC_API_SERVER_URL'])
      { success: true, data: JSON.parse(response) }
    rescue JSON::ParserError
      { success: false, message: 'Invalid Response!' }
    rescue Net::OpenTimeout, Net::HTTPRequestTimeOut
      { success: false, message: 'Request Timeout!' }
    end
  end
end
