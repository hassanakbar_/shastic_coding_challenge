# frozen_string_literal: true

module Statistics
  class CreateService
    def initialize(statistics)
      @statistics = statistics
    end

    def call
      success_count = 0
      failure_reasons = []
      @statistics.each do |visit_details|
        result = ::Visits::CreateService.new(visit_details).call
        if result[:success]
          success_count += 1
        else
          failure_reasons << { visit_details['idVisit'] => result[:message] }
        end
      end
      {
        success: true,
        data: {
          success_count: success_count,
          total: @statistics.count,
          failure_reasons: failure_reasons
        }
      }
    end
  end
end
