# frozen_string_literal: true

module Visits
  class CreateService
    def initialize(params)
      @params = params
    end

    def call
      ActiveRecord::Base.transaction do
        begin
          visit = Visit.create!(visit_params)
          visit.page_views << page_views
          { success: true, data: visit }
        rescue ActiveRecord::RecordInvalid => e
          { success: false, message: e.record.errors.full_messages.join(', ') }
        end
      end
    end

    private

    def visit_params
      Visits::FIELDS_MAPPING.map do |key, value|
        [value, @params[key]]
      end.to_h
    end

    def page_view_params(page_details)
      PageViews::FIELDS_MAPPING.map do |key, value|
        [value, page_details[key]]
      end.to_h
    end

    def page_views
      uniq_page_views = @params['actionDetails'].uniq { |p| p['pageId'] }
      uniq_page_views.map.with_index do |page_details, index|
        PageView.new(page_view_params(page_details).merge(position: index))
      end
    end
  end
end
