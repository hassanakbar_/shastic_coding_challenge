# frozen_string_literal: true

require_relative 'visits'
require_relative 'visits/referrer'

class Visit < ActiveRecord::Base
  has_many :page_views, dependent: :destroy
  validates :evid, format: { with: ::Visits::Referrer::NAME_FORMAT }

  before_validation :cleanup_evid, if: :new_record?

  def cleanup_evid
    evid.gsub!('evid_', '')
  end
end
