# frozen_string_literal: true

module PageViews
  FIELDS_MAPPING = {
    'url' => :url,
    'pageTitle' => :title,
    'timeSpent' => :time_spent,
    'timestamp' => :timestamp
  }.freeze
end
