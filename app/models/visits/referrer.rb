# frozen_string_literal: true

require_relative '../visits'

module Visits
  class Referrer
    NAME_FORMAT = /\A[A-z0-9]{8}-[A-z0-9]{4}-[A-z0-9]{4}-[A-z0-9]{4}-[A-z0-9]{12}\z/.freeze
  end
end
