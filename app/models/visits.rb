# frozen_string_literal: true

module Visits
  FIELDS_MAPPING = {
    'referrerName' => :evid,
    'idSite' => :vendor_site_id,
    'idVisit' => :vendor_visit_id,
    'visitIp' => :visit_ip,
    'visitoriId' => :vendor_visitor_id
  }.freeze
end
