# frozen_string_literal: true

class PageView < ActiveRecord::Base
  belongs_to :visit
  default_scope { order(:timestamp) }
end
