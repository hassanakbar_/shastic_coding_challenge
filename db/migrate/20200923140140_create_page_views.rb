# frozen_string_literal: true

# PageViews Migration
class CreatePageViews < ActiveRecord::Migration[6.0]
  def change
    create_table :page_views do |t|
      t.bigint 'visit_id', index: true
      t.string 'title'
      t.string 'position'
      t.text 'url'
      t.string 'time_spent'
      t.decimal 'timestamp', precision: 14, scale: 3

      t.timestamps null: false
    end
  end
end
